package pl.arcsoftware.carcoderepo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarCodeRepoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarCodeRepoApplication.class, args);
    }

}
